USE clublambda;

# Création de la table seances
CREATE TABLE seances(
    id                  INT PRIMARY KEY AUTO_INCREMENT,
    titre               VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    description         TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    heureDebut          TIME NOT NULL,
    date                DATE NOT NULL,
    duree               INT NOT NULL,
    nbParticipantsMax   INT NOT NULL,
    couleur             VARCHAR(255)
);

# Création de la table users
CREATE TABLE users(
    id                  INT PRIMARY KEY AUTO_INCREMENT,
    nom                 VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    email               VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    password            VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    isAdmin             TINYINT NOT NULL,
    isActif             TINYINT NOT NULL,
    token               VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
);


# Création de la table de jointure pour les inscrits
CREATE TABLE inscrits(
    id_seance           INT,
    id_user             INT,
    PRIMARY KEY         (id_seance, id_user),
    FOREIGN KEY         (id_seance) REFERENCES seances(id),
    FOREIGN KEY         (id_user) REFERENCES users(id)
);