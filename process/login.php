<?php
// Ce fichier sert à activer les données du formulaire de login

// On va utiliser la sessions pour passer des messages d'une page à l'autre
// Pour cela, il faut démarrer la session au débutg des pages concernées
session_start();

require_once(__DIR__."/../model/Database.php");
$database = new Database();

// On récupère les données dans l'url
$email = isset($_POST["email"]) ? $_POST["email"] : null;
$password = isset($_POST["password"]) ? $_POST["password"] : null;

// On fait les vérifications d'usage
if($email == null || $password == null){
    $_SESSION["error"] = "L'email et le mot de passe sont obligatoire.";
    header("location: ../vues/login.php");
    exit();
}

// On récupère le user en BD
$user = $database->getUserByEmail($email);
// On vérifie que le user a bien été retrouvé
if(!$user){
    $_SESSION["error"] = "L'email est incorrect, vous n'avez pas été trouvé.";
    header("location: ../vues/login.php");
    exit();
}

// On vérifie que le user est actif
if($user->isActif() == 0){
    $_SESSION["error"] = "Votre compte n'a pas encore été validé, consultez vos emails.";
    header("location: ../vues/login.php");
    exit();
}

// On vérifie le mot de passe
if(!password_verify($password, $user->getPassword())){
    $_SESSION["error"] = "Le mot de passe est incorrect";
    header("location: ../vues/login.php");
}

// Tous les test ont réussi donc on peut logger le user
// On ajoute son id et l'objet dans la session
$_SESSION["id"] = $user->getId();
$_SESSION["user"] = serialize($user);

// On redirige vers la page planning
$_SESSION["info"] = "Vous êtes bien connecté";
header("location: ../vues/planning.php");
