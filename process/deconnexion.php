<?php
// Ce fichier sert à se déconnecter

// On va utiliser la sessions pour passer des messages d'une page à l'autre
// Pour cela, il faut démarrer la session au débutg des pages concernées
session_start();

// On vide la session
$_SESSION = [];

// On redirige vers la page d'accueil
header("location: ../vues/index.php");
exit();
