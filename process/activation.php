<?php
// Ce fichier sert à activer un utilisateur qui vient de s'inscrire

// On va utiliser la sessions pour passer des messages d'une page à l'autre
// Pour cela, il faut démarrer la session au débutg des pages concernées
session_start();

require_once(__DIR__."/../model/Database.php");
$database = new Database();

// On récupère les données dans l'url
$idUser = isset($_GET["id"]) ? $_GET["id"] : null;
$token = isset($_GET["token"]) ? $_GET["token"] : null;

// On fait les vérifications d'usage
if($idUser == null || $token == null){
    $_SESSION["error"] = "Un problème est survenue lors de votre inscription, veuilez recommencer.";
    header("location: ../vues/inscription.php");
    exit();
}

// On cherche l'utilisateur dans la BD grâce à son id
$user = $database->getUserById($idUser);
// On vérifie que le user a bien été retrouvé
if(!$user){
    $_SESSION["error"] = "Un problème est survenue lors de votre inscription, veuilez recommencer.";
    header("location: ../vues/inscription.php");
    exit();
}

// On compare les tokens pour authentifier l'utilisateur
if($token != $user->getToken()){
    $_SESSION["error"] = "Un problème est survenue lors de votre inscription, veuilez recommencer.";
    header("location: ../vues/inscription.php");
    exit();
}

// Si tout s'est bien passé, on active l'utilisateur
if($database->activateUser($idUser)){
    // Puis on redirige vers la page de login avec un message de succès
    $_SESSION["info"] = "Votre compte a bien été activé, vous pouvez vous connecter.";
    header("location: ../vues/login.php");
}else{
    // Si l'activation a échoué, on renvoit vers la page d'inscription
    $_SESSION["error"] = "Un problème est survenue lors de votre inscription, veuilez recommencer.";
    header("location: ../vues/inscription.php");
}