<?php
// Ce fichier sert à supprimer une séance

// On va utiliser la session pour passer des messages d'une pagee à l'autre
// Pour cela il faut démarrer la session au début des pages concernées
session_start();

require_once(__DIR__."/../model/Database.php");
$database = new Database();

// Récupération de l'id dans l'url
$idSeance = $_GET["id"];

// Au cas ou, vérifions que nous avons bien un id
if(!$idSeance){
    // Si nous n'en avons pas, il faut revenir à la page précédente avec un message d'erreur
    $_SESSION["error"] = "Le lien de suppression n'est pas correct";
    header("location: ../vues/planning.php");
}

// Si tout va bien, on supprime la séance
if($database->deleteSeance($idSeance)){
    // La séance a bien été supprimée
    $_SESSION["info"] = "Séance supprimée avec succès";
    header("location: ../vues/planning.php");
}else{
    // La séance n'a pas pu être supprimée
    $_SESSION["info"] = "Le lien de suppression n'est pas correct";
    header("location: ../vues/cours.php?id=".$id);
}