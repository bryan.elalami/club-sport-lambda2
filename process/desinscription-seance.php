<?php
// Ce fichier sert à désinscrire un user à une séance

// On va utiliser la sessions pour passer des messages d'une page à l'autre
// Pour cela, il faut démarrer la session au débutg des pages concernées
session_start();

require_once(__DIR__."/../model/Database.php");
$database = new Database();

// Récupérer l'id de la séance dans l'url
$idSeance = $_GET["id"];

// Récupérer l'id du user dans la session
$idUser = $_SESSION["id"];

// Effectuer l'inscription en BD
if($database->deleteParticipant($idSeance, $idUser)){
    // Si ça s'est bien passé
    $_SESSION["info"] = "Vous avez bien été désinscrit de cette séance";
}else{
    $_SESSION["error"] = "Nous n'avons pas réussi à vous désinscrire à cette séance";
}
header("location: ../vues/cours.php?id=".$idSeance);
exit();