<?php

// Ceci remplace l'instruction quand on a défini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__ ."/../model/Seance.php");
include_once(__DIR__ ."/../model/User.php");
include_once(__DIR__ ."/../model/Database.php");

final class SeanceTest extends Testcase {

    public function testCreateSeance(){
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

        $database = new Database();

        $this->assertNotFalse($database->createSeance($seance));
    }

    public function testGetSeanceById(){
        $database = new Database();
        // Je crée et j'insert une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $id = $database->createSeance($seance);
        // Je certifie que la séance existe
        $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));
    }

    public function testGetSeanceByWeek(){
        $database = new Database();
        // Je crée et j'insert une séance à la date d'aujourd'hui
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // J'insère la séance en vérifiant que ça s'est bien passé
        $this->assertNotFalse($database->createSeance($seance));
        // Je compte le nombre de séance en lui passant le numéro de la semaine courante
        $nbSeances = count($database->getSeanceByWeek(date("W")));
        echo($nbSeances);
        // Et je vérifie qu'il y a au moins une séance dans la Database
        $this->assertGreaterThan(0, $nbSeances);
    }

    public static function tearDownAfterClass(){
        $database = new Database();
        $database->deleteAllSeance();
    }

    public function testUpdateSeance(){
        $database = new Database();
        // Je crée une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $id = $database->createSeance($seance);
        // Je récupère la séance en base de données
        $seance = $database->getSeanceById($id);
        $this->assertInstanceOf(Seance::class, $seance);
        // Je modifie la séance
        $seance->setTitre("Yoga");
        // Je vérifie que la séance est mise à jour
        $this->assertTrue($database->updateSeance($seance));
    }

    public function testDeleteSeance(){
        $database = new Database();
        // Je crée une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $id = $database->createSeance($seance);
        // Je supprime la séance et vérifie le résultat
        $this->assertTrue($database->deleteSeance($id));
    }

    public function testInsertDeleteParticipant(){
        $database = new Database();
        // Je crée un user
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                            0, 0, bin2hex(random_bytes(20)));
        // Je le sauvegarde en database et vérifie que tout s'est bien passé
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);
        // Je crée une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $idSeance = $database->createSeance($seance);
        $this->assertNotFalse($idSeance);
        // J'insère un participant
        $this->assertTrue($database->insertParticipant($idSeance, $idUser));
        // Je supprime le participant
        $this->assertTrue($database->deleteParticipant($idSeance, $idUser));
    }


    public function testEmailAlreadyExists(){
        $database = new Database();
        // Je crée un user
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                            0, 0, bin2hex(random_bytes(20)));
        // L'insérer puis vérifier que tout s'est bien passé
        $this->assertNotFalse($database->createUser($user));
        // Vérifie un email qui existe
        $emailTrue = "toto@gmail.com";
        $this->assertTrue($database->isEmailExists($emailTrue));
        // Vérifier un email qui n'existe pas
        $emailFalse = "toto@hotmail.com";
        $this->assertFalse($database->isEmailExists($emailFalse));
    }


    public function testGetUserByEmail(){
        $database = new Database();
        // Je crée un user
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                            0, 0, bin2hex(random_bytes(20)));
        // L'insérer puis vérifier que tout s'est bien passé
        $this->assertNotFalse($database->createUser($user));
        // Vérifie qu'on récupère qu'on récupère bien le user grâce à son email
        $emailTrue = "toto@gmail.com";
        $this->assertInstanceOf(User::class, $database->getUserByEmail($emailTrue));
        // Vérifier qu'on ne récupère personne si l'email n'existe pas
        $emailFalse = "toto@hotmail.com";
        $this->assertFalse($database->getUserByEmail($emailFalse));
    }


    public function testGetSeanceByUserId(){
        $database = new Database();
        // Je crée un user
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                            0, 0, bin2hex(random_bytes(20)));
        // Je le sauvegarde en base de données et vérifie que tout s'est bien passé
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);
        // Je vérifie que je récupère un tableau vide de séance pour ce user
        $this->assertEquals(0, count($database->getSeanceByUserId($idUser)));
        // Je crée une séance
        $seance1 = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $idSeance1 = $database->createSeance($seance1);
        $this->assertNotFalse($idSeance1);
        // J'inscrit l'utilisateur à cette séance
        $this->assertTrue($database->insertParticipant($idSeance1, $idUser));
        //Je vérifie que je récupère un tableau avec une séance pour ce user
        $this->assertEquals(1, count($database->getSeanceByUserId($idUser)));
        // Je crée une deuxième séance
        $seance2 = Seance::createSeance("Yoga", "Ce cours détend", "10:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $idSeance2 = $database->createSeance($seance2);
        $this->assertNotFalse($idSeance2);
        // J'inscrit l'utilisateur à cette séance
        $this->assertTrue($database->insertParticipant($idSeance2, $idUser));
        //Je vérifie que je récupère un tableau avec une séance pour ce user
        $this->assertEquals(2, count($database->getSeanceByUserId($idUser)));   
    }

    

    
        
}