<?php

// Ceci remplace l'instruction include quand on défini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__ ."/../model/Seance.php");
include_once(__DIR__ ."/../model/User.php");
include_once(__DIR__ ."/../model/Database.php");

final class UserTest extends TestCase {

    public function testCreateUser(){
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));
        
        $database = new Database();

        $this->assertNotFalse($database->createUser($user));
    }

    public static function tearDownAfterClass(){
        $database = new Database();
        $database->deleteAllInscrit();
        $database->deleteAllUser();
        $database->deleteAllSeance();
    }
    
    public function testGetAndActivateUser(){
        $database = new Database();
        // Créer le user
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));
        // L'insérer et récupérer son id puis vérifier que tout s'est bien passé
        $id = $database->createUser($user);
        $this->assertNotFalse($id);
        // Activer le user
        $this->assertTrue($database->activateUser($id));
        // Récupérer le user via son id
        $user = $database->getUserById($id);
        $this->assertInstanceOf(User::class, $user);
        // Vérifier que le user est actif
        $this->assertEquals(1, $user->isActif());
    }

}

