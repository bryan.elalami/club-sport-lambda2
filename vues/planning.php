<?php 
    include('modules/partie1.php');
?>
<?php
    require_once(__DIR__."/../model/Database.php");
    $database = new Database();

    // Trouver le chiffre de la semaine courante(actuelle)
    $weekNumber = date("w");
    
    // Chercher les séances de cette semaine
    $seancesOfWeek = $database->getSeanceByWeek($weekNumber);

    // On crée les index associés aux jours "w" voir doc PHP
    const LUNDI = 1;
    const MARDI = 2;
    const MERCREDI = 3;
    const JEUDI = 4;
    const VENDREDI = 5;
    const SAMEDI = 6;
    
    // Ranger les séances par jour de la semaine en commençant par le Lundi
    $seances = [];
    $seances[LUNDI] = [];
    $seances[MARDI] = [];
    $seances[MERCREDI] = [];
    $seances[JEUDI] = [];
    $seances[VENDREDI] = [];
    $seances[SAMEDI] = [];
    
    $seancesOfWeek = $database-> getSeanceByWeek(date("W"));
    //var_dump($seancesOfWeek);
    foreach($seancesOfWeek as $seance){
        //var_dump($seance);
        // On détermine le jour de la séance
        $indexDay = date("w", strtotime($seance->getDate()));
        //var_dump($indexDay);
        // On ajoute la séance dans un tableau assoicé au numéro du jour de la semaine
        array_push($seances[$indexDay], $seance);
    }
    //var_dump($seances);
?>

<div class="container-card text-center mt-4">
    <h1 class="card-header">Planning de la semaine</h1>
    <div class="card-body">
        <div class="row">
            <div id="lundi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Lundi</h3>
                <?php
                    foreach($seances[LUNDI] as $seance){
                        include('modules/etiquette.php');
                    }
                ?>
            </div>
            <div id="mardi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Mardi</h3>
                    <?php
                    foreach($seances[MARDI] as $seance){
                        include('modules/etiquette.php');
                    }
                ?>
            </div>
            <div id="mercredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Mercredi</h3>
                <?php
                    foreach($seances[MERCREDI] as $seance){
                        include('modules/etiquette.php');
                    }
                ?>
            </div>
            <div id="jeudi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Jeudi</h3>
                <?php
                    foreach($seances[JEUDI] as $seance){
                        include('modules/etiquette.php');
                    }
                ?>
            </div>
            <div id="vendredi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Vendredi</h3>
                <?php
                    foreach($seances[VENDREDI] as $seance){
                        include('modules/etiquette.php');
                    }
                ?>
            </div>
            <div id="samedi" class="col-6 col-md-4 col-lg-2 border border-primary border-top-0">
                <h3>Samedi</h3>
                <?php
                    foreach($seances[SAMEDI] as $seance){
                        include('modules/etiquette.php');
                    }
                ?>
            </div>
        </div>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>