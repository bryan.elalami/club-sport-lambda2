<?php
// On va utiliser la session pour passer des messages d'une page à l'autre
// Pour cela, il faut démarrer la session au début des pages
session_start();
?>


<!DOCTYPE html>
<html lang="fr">

<?php
    include('header.php');
?>

<body>

<?php
    include('navbar.php');
    include('message.php');
?>