<?php
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue chez le club Lambda</h1>
    <div class="card-body">
        <img class="mainImage" src="/vues/assets/images/fitness.jpg">
        <p class="p-4">
        Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression.
        Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme
        assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait
        que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié.
        Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment,
        par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.
        Pourquoi l'utiliser? On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions,
        et empêche de se concentrer sur la mise en page elle-même. L'avantage du Lorem Ipsum sur un texte générique comme 'Du texte. Du texte.
        Du texte.' est qu'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard.
        </p>
        <a class="btn btn-dark" href="#">Consulter le planning</a>
    </div>
</div>

<?php
    include('modules/partie3.php');
?>