<?php
    include('modules/partie1.php');
?>
<?php
// Import et distanciation de la classe Datab
require_once(__DIR__."/../model/Database.php");
$database = new Database();

// On redirige l'uutiisateur vers la page de login s'il n'est pas conenecté
if(!isset($_SESSION["user"])){
    // Comme notre page est déjà partielleent construite on ne peut pas utiliser header()
    // On va donc utiliser un petit script javascript pour se rediriger
    echo '<script type="text/javascript">';
    echo 'window.location.href="login.php";';
    echo '</script>';
}
// Recherche du user dans la session et désérialisation
$user = unserialize($_SESSION["user"]);

// Récupérer les séances en BD en fonction de l'utilisateur
$seances = $database->getSeanceByUserId($user->getId());

?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Bienvenue <?php echo $user->getNom();?></h1>
    <div class="card-body text-left">
        <div class="card-title">
            Vous êtes inscrits aux cours suivants : 
        </div>
        <div class="card-text">
            <ul class="inscrit text-dark text-left">
                <?php foreach($seances as $seance){ ?>
                <a href="/vues/cours.php?id=<?php echo $seances->getId();?>"><li><i class="fa fa-bookmark"></i>
                <?php echo $seance->getTitre().", le ".date("d/m/Y", strtotime($seance->getDate()))." à ".date("G\hi", strtotime($seance->getHeureDebut())) ?></li></a>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<?php
    include('modules/partie3.php');
?>