<?php

/** 
 * Classe de connexion à la base de donnée
*/

    include_once('Seance.php');
    include_once('User.php');



class Database{

    //Constantes de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "@!Mr8Qr36p";

    //Attribut de la Classe
    private $connexion;

    //Constructeur pour initier la connexion
    public function __construct(){
        try {
            $this->connexion = new
            PDO("mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8",
                                    self::DB_USER,
                                    self::DB_PASSWORD);
        } catch (PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
        }
    }

    /** 
 * Fonction pour créer une nouvelle séance en base de données
 * 
 * @param{Seance} seance : la séance à sauvegarder
 * 
 * @return{integer, boolean} l'id si la séance a été créee ou false sinon
*/
public function createSeance(Seance $seance){
    //Je prépare ma requête SQL
    $pdoStatement = $this->connexion->prepare(
        "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
        VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
    );
    //var_dump($seance);
    //J'execute ma requête en passant les valeurs de l'objet Seance
    $pdoStatement->execute([
        "titre"             => $seance->getTitre(),
        "description"       => $seance->getDescription(),
        "heureDebut"        => $seance->getHeureDebut(),
        "date"              => $seance->getDate(),
        "duree"             => $seance->getDuree(),
        "nbParticipantsMax" => $seance->getNbParticipantsMax(),
        "couleur"           => $seance->getCouleur()
    ]);

    //var_dump($pdoStatement->errorInfo());
  
    //Je récupère l'id créé si l'exécution s'est bien passée (code 0000 de Mysql)
        if($pdoStatement->errorCode()== 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else{
            return false;
            
        }
    }


    /**
     * 
     * Cette fonction cherche la séance dont l'id est passé en paramètre et la retourne
     * 
     * @param{integer} id: l'id de la séance recherchée
     * 
     * @param {Seance|boolean} : un objet Seance si la séance a été trouvée, false sinon
     */
    public function getSeanceById($id){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM seances WHERE id = :id"
        );
        // J'exéctue ma requête SQL
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Je récupère le résultat
        $seance = $pdoStatement->fetchObject("Seance");
        return $seance;
    }

    /**
     * Fonction retourne toutes les séances de la semaine
     * 
     * @param{integer} week : le numéro de la semaine recherchée
     * 
     * @return{array} : un tableau de Séance sil y a des séances programmées pour cette semaine
     */
    public function getSeanceByWeek($week){
        // Je préapre ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM `seances` WHERE WEEKOFYEAR(date) = :week
            ORDER BY  date, heureDebut"
            
        );
        // J'exécute la requête en lui passant le numér de la semaine
        $pdoStatement->execute(
            ["week" => $week]
        );
        // Je récupère les résultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS,"Seance");
        //var_dump($pdoStatement->errorInfo());
        return $seances;
    }

    public function deleteAllSeance(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances;"
        );
        $pdoStatement->execute();
    }

    /**
     * Fonction pour mettre à jour une séance en base de données
     * 
     * @param{Seance} seance : la séance à mettre à jour
     * 
     * @return {boolean} true si la séance est mise à jour ou false sinon
     */
    public function updateSeance(Seance $seance){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
        "UPDATE seances
        SET titre = :titre, description = :description, heureDebut = :heureDebut, date = :date, duree = :duree, nbParticipantsMax = :nbParticipantsMax, couleur = :couleur
        WHERE id = :id"
        );
        // J'exécute ma requête en apssant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"             => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut"        => $seance->getHeureDebut(),
            "date"              => $seance->getDate(),
            "duree"             => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur"           => $seance->getCouleur(),
            "id"                => $seance->getId()
        ]);
    // Retourne true crée si l'exécution s'est bien passée (code 0000 de MySQL)
    if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Fonction pour supprimer une séance en base de données
     * 
     * @param{integer} id : l'id de la séance à suppprimer
     * 
     * @return{boolean} true si la séance est suprrimée ou false sinon
     */
    public function deleteSeance($id){
        // Je prépare la requête pour supprimer tous les inscrits à la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_seance = :seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["seance" => $id]
        );
        // Si ça ne s'est pas bien passé, ce n'est pas la peine de continuer
        if($pdoStatement->errorCode() != 0){
            return false;
        }
        // Si les inscrits sont supprimés, je prépare la requête pour supprimer la séance
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM seances WHERE id = :seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["seance" => $id]
        );
        // Retourne true crée si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    public function insertParticipant($idSeance, $idUser){
        // Je prépare la requête d'insertion
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO inscrits (id_user, id_seance) VALUES (:id_user, :id_seance)"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // Retourne true crée si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            //var_dump($pdoStatement->errorInfo());
            return false;
        }
    }

    public function deleteParticipant($idSeance, $idUser){
        // Je prépare la requête d'insertion
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
        // J'exécute ma requête
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // Retourne true crée si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param{User} user : le user à sauvegarder
     * 
     * @return{integer, boolean} l'id si le user a été crée ou false sinon
     */
    public function createUser(User $user){
         // Je prépare ma requête SQL
         //var_dump($user);
         $pdoStatement = $this->connexion->prepare(
            "INSERT INTO users(nom, email, password, isAdmin, isActif, token)
            VALUES (:nom, :email, :password, :isAdmin, :isActif, :token)"
        );
        // j'exécute ma requête en passant les valeurs de l'objet User en valeur
        $pdoStatement->execute([
            "nom"               => $user->getNom(),
            "email"             => $user->getEmail(),
            "password"          => $user->getPassword(),
            "isAdmin"           => $user->isAdmin(),
            "isActif"           => $user->isActif(),
            "token"             => $user->getToken()
        ]);
        // Je récupère l'id crée si l'exécution s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        }else{
            //$pdoStatement->errorCodevar_dump($pdoStatement->errorInfo());
            return false;
        }
    }

    public function deleteAllUser(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM users;"
        );
        $pdoStatement->execute();
    }
    public function deleteAllInscrit(){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "DELETE FROM inscrits;"
        );
        $pdoStatement->execute();
    }


    /**
     * 
     * Cette fonction cherche le user dont l'id est passé en paramètre et le retourne
     * 
     * @param{integer} id: l'id du recherché
     * 
     * @return {User|boolean} : un objet User si le user a été trouvée, false sinon
     */
    public function getUserById($id){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE id = :id"
        );
        // J'exéctue ma requête SQL
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Je récupère le résultat
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }
/**
     * 
     * Vérifie si un email existe déjà dans la table users
     * 
     * @param{string} email: un email utilisé pour s'inscrire
     * 
     * @return {User|boolean} : true si l'email existe déjà, false sinon
     */
    public function isEmailExists($email){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM users WHERE email = :email"
        );
        // J'exécute ma requête en passant l'email en valeur
        $pdoStatement->execute([
            "email" => $email
        ]);
        // Je récupère le résultat
        $nbUser = $pdoStatement->fetchColumn();
        // Si l'email n'a pas été retrouvé, retourner false)
        if($nbUser == 0){
            return false;
        }else{
            // l'email a été trouvé
            return true;
        }
    }


    /**
     * 
     * Cette fonction cherche le user dont l'email est passé en paramètre et le retourne
     * 
     * @param{integer} id: l'email du user du recherché
     * 
     * @return {User|boolean} : un objet User si le user a été trouvée, false sinon
     */
    public function getUserByEmail($email){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE email = :email"
        );
        // J'exéctue ma requête SQL
        $pdoStatement->execute(
            ["email" => $email]
        );
        // Je récupère le résultat
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }


    /**
     * 
     * Fonction qui permet de retrouver toutes les séances auxquelles est inscrit le user
     * 
     * @param{integer} id: l'id du user du concerné
     * 
     * @return {array} : un tableau conteantn toutes les séances
     */
    public function getSeanceByUserId($idUser){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT s.* FROM seances s INNER JOIN inscrits i ON s.id = i.id_seance WHERE i.id_user = :id_user"
        );
        // J'exéctue la requête en lu ipassant l'id du user
        $pdoStatement->execute(
            ["id_user" => $idUser]
        );
        // Je récupère les résultats
        $seances = $pdoStatement->fetchAll(PDO::FETCH_CLASS, "Seance");
        return $seances;
    }


    /**
     * 
     * Cette fonction cherche le user dont l'id est passé en paramètre et le retourne
     * 
     * @param{integer} id: l'id du user recherché
     * 
     * @return {USER |boolean} : un objet User si le user a été trouvé, false sinon
     */
    public function getUserByUserId($id){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT * FROM users WHERE id = :id"
        );
        // J'exéctue la requête en lui passant l'id
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Je récupère les résultats
        $user = $pdoStatement->fetchObject("User");
        return $user;
    }

    /**
     * 
     * Active le user dont l'id est passée en paramètre
     * 
     * @param{integer} id: l'id du user à activer
     * 
     * @return {boolean} : true si l'activation s'est bien passée, false sinon
     */
    public function activateUser($id){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "UPDATE users
            SET isActif = 1
            WHERE id= :id"
        );
        // J'exéctue la requête en passant l'id en valeur
        $pdoStatement->execute(
            ["id" => $id]
        );
        // Retourne true crée si l'exécution s'es bien passée (code 0000 de MySQL)
        if($pdoStatement->errorCode() == 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 
     * Fonction qui permet de savoir si un user est inscrit à une séance
     * 
     * @param{integer} id: l'id du user
     * @param{integer} id: l'id de la séance
     * 
     * @return {boolean} : true si le user est inscrit false sinon
     */
    public function isInscrit($idUser, $idSeance){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_user = :id_user AND id_seance = :id_seance"
        );
        // J'exéctue la requête en passant l'id en valeur
        $pdoStatement->execute(
            ["id_user" => $idUser,
            "id_seance" => $idSeance]
        );
        // Je récupère le résultat
        $inscrit = $pdoStatement->fetchColumn();
        // Si aucune inscription n'a été trouvée, retourner false
        if($inscrit == 0){
            return false;
        }else{
            // Une inscription a été trouvée
            return true;
        }
    }

    /**
     * 
     * Fonction qui retourne le nombre d'inscrits à une séance
     * 
     * @param{integer} id: l'id de la séance
     * 
     * @return {boolean} : le nombre d'inscrits
     */
    public function nbInscrits($idSeance){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "SELECT COUNT(*) FROM inscrits WHERE id_seance = :id_seance"
        );
        // J'exéctue la requête en passant l'id en valeur
        $pdoStatement->execute(
            ["id_seance" => $idSeance]
        );
        // Je récupère le résultat
        $nbInscrits = $pdoStatement->fetchColumn();
        // Retourne le nombre d'inscrits à cette séance
            return $nbInscrits;
        }
    }


